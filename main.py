from evengsdk.client import EvengClient
import xml.etree.ElementTree as ET
from tabulate import tabulate
import json
from datetime import datetime


def parseXML_for_node_details(xmlfile):

    tree = ET.parse(xmlfile)

    root = tree.getroot()

    nodes_details = []

    # Iterate for nodes
    for nodes in root.findall('./topology/nodes'):

        for child in nodes:
            node_details = {
                "name": child.attrib["name"],
                "id": child.attrib["id"],
                "template": child.attrib["template"],
                "image": child.attrib["image"]
            }

            nodes_details.append(node_details)

    return nodes_details


def check_for_folder(folder_list, folder):
    automation_folder_exist = False
    for line in folder_list:
        if line['name'] == folder:
            automation_folder_exist = True
            return True
    return False


if __name__ == "__main__":
    client = EvengClient("192.168.1.208", log_file="test.log", ssl_verify=False, protocol="http")
    client.disable_insecure_warnings()
    client.login(username="admin", password="eve")
    client.set_log_level("DEBUG")

    print(client.api.list_folders()['data'])
    print(client.api.list_networks())
    print(client.api.list_users())

    lab_nodes = parseXML_for_node_details('lab_base/IOX Test.unl')
    print(tabulate(lab_nodes, headers='keys'))

    # List folders and look for folder called automation

    folder_dict = client.api.list_folders()['data']['folders']

    folder_name = "Automation_labs11"
    if check_for_folder(folder_dict, folder_name):
        client.log.info(f"{datetime.now()} - {folder_name} folder exist.")
    else:
        client.log.info(f"{datetime.now()} - {folder_name} folder does not exist.  Creating folder.")
        folder_data = {"path": "", "name": folder_name}
        client.post("/folders", data=json.dumps(folder_data))



